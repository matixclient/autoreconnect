package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.gui.DisplayGuiScreenEvent;
import de.paxii.clarinet.event.events.gui.RenderGuiScreenEvent;
import de.paxii.clarinet.gui.menu.hooks.GuiAutoReconnect;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.Chat;
import de.paxii.clarinet.util.settings.type.ClientSettingInteger;

import net.minecraft.client.gui.GuiDisconnected;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.multiplayer.GuiConnecting;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.util.text.ITextComponent;

import java.lang.reflect.Field;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Lars on 24.06.2016.
 */
public class ModuleAutoReconnect extends Module {
  @Getter
  private static ModuleAutoReconnect instance;
  private ServerData serverData;
  @Getter
  @Setter
  private boolean shouldReconnect = false;
  @Getter
  @Setter
  private boolean reconnecting = false;
  private GuiScreen parentScreen;

  public ModuleAutoReconnect() {
    super("AutoReconnect", ModuleCategory.OTHER);

    ModuleAutoReconnect.instance = this;
    this.setEnabled(true);
    this.setDisplayedInGui(false);
    this.setCommand(true);
    this.setVersion("1.0");
    this.setBuildVersion(16001);
    this.setDescription("Automatically reconnects to a server when you get disconnected");
    this.setSyntax("autoreconnect delay <time in seconds>");

    this.getModuleSettings().put("delay", new ClientSettingInteger("delay", 5000));
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);
  }

  @EventHandler
  public void onDisplayGuiScreen(DisplayGuiScreenEvent screenEvent) {
    if (this.parentScreen == null) {
      this.parentScreen = new GuiMultiplayer(new GuiMainMenu());
    }

    if (screenEvent.getGuiScreen() instanceof GuiConnecting) {
      this.serverData = Wrapper.getMinecraft().getCurrentServerData();
    } else if (screenEvent.getGuiScreen() instanceof GuiDisconnected) {
      GuiDisconnected guiDisconnected = (GuiDisconnected) screenEvent.getGuiScreen();
      screenEvent.setGuiScreen(this.displayReconnectScreen(guiDisconnected));
    }
  }

  @EventHandler
  public void onRenderGuiScreen(RenderGuiScreenEvent event) {
    if (shouldReconnect) {
      if (Wrapper.getMinecraft().currentScreen instanceof GuiAutoReconnect) {
        Wrapper.getMinecraft().displayGuiScreen(new GuiConnecting(this.parentScreen, Wrapper.getMinecraft(), ModuleAutoReconnect.this.serverData));
      }
      shouldReconnect = false;
    }
  }

  @Override
  public void onCommand(String[] args) {
    if (args.length > 1) {
      if (args[0].equalsIgnoreCase("delay")) {
        try {
          int delay = Integer.parseInt(args[1]);
          this.getModuleSettings().put("delay", new ClientSettingInteger("delay", delay * 1000));

          Chat.printChatMessage(String.format("AutoReconnect Delay has been set to %d Seconds.", delay));
        } catch (NumberFormatException e) {
          Chat.printClientMessage("Invalid Syntax!");
        }
      } else {
        Chat.printClientMessage("Unknown subcommand!");
      }
    } else {
      Chat.printClientMessage("Too few arguments!");
    }
  }

  private GuiAutoReconnect displayReconnectScreen(GuiDisconnected guiDisconnected) {
    try {
      Field message = guiDisconnected.getClass().getDeclaredField("message");
      Field reason = guiDisconnected.getClass().getDeclaredField("reason");
      message.setAccessible(true);
      reason.setAccessible(true);
      ITextComponent textComponent = (ITextComponent) message.get(guiDisconnected);
      String reasonMessage = (String) reason.get(guiDisconnected);

      return new GuiAutoReconnect(this.parentScreen, reasonMessage, textComponent);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return (GuiAutoReconnect) guiDisconnected;
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);
  }
}