package de.paxii.clarinet.gui.menu.hooks;

import de.paxii.clarinet.module.external.ModuleAutoReconnect;
import de.paxii.clarinet.util.chat.ChatColor;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiDisconnected;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.text.ITextComponent;

import java.io.IOException;

/**
 * Created by Lars on 24.06.2016.
 */
public class GuiAutoReconnect extends GuiDisconnected {
  private GuiScreen parentScreen;
  private GuiButton reconnectButton;
  private ModuleAutoReconnect moduleAutoReconnect;

  public GuiAutoReconnect(GuiScreen screen, String reasonLocalizationKey, ITextComponent chatComp) {
    super(screen, reasonLocalizationKey, chatComp);

    this.parentScreen = screen;
    this.moduleAutoReconnect = ModuleAutoReconnect.getInstance();
    this.setReconnecting();
  }

  @Override
  public void initGui() {
    super.initGui();

    this.buttonList.add(this.reconnectButton = new GuiButton(1, this.width / 2 - 100, this.height - 30, this.getButtonString()));
  }

  @Override
  protected void actionPerformed(GuiButton button) throws IOException {
    if (button.id == 0) {
      this.mc.displayGuiScreen(this.parentScreen);
    } else if (button.id == 1) {
      this.moduleAutoReconnect.setReconnecting(!this.moduleAutoReconnect.isReconnecting());
      this.reconnectButton.displayString = this.getButtonString();

      this.setReconnecting();
    }
  }

  private String getButtonString() {
    return String.format("Auto-Reconnect: %s", this.moduleAutoReconnect.isReconnecting() ? ChatColor.GREEN + "On" : ChatColor.RED + "Off");
  }

  private void setReconnecting() {
    if (ModuleAutoReconnect.getInstance().isReconnecting()) {
      new Thread(() -> {
        try {
          Thread.sleep(5000L);
        } catch (InterruptedException e) {
          e.printStackTrace();
        } finally {
          ModuleAutoReconnect.getInstance().setShouldReconnect(true);
        }
      }).start();
    }
  }
}